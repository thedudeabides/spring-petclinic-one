FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
#RUN mvn package

ARG PORT
ENV PORT ${PORT:-5000}
EXPOSE $PORT

#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
CMD [ "sh", "-c", "java -Dserver.port=$PORT -jar target/spring-petclinic-2.1.0.jar" ]
